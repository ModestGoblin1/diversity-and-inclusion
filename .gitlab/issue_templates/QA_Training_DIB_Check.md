# Diversity, Inclusion and Belonging, Quality Check for Learning & Development and External Training

## Before starting the QA Check:

- Have you been a part of developing the training or choosing the training provider?
- Have you completed the [Diversity, Inclusion, and Belonging Certification](https://about.gitlab.com/company/culture/inclusion/dib-training/) and Bias training to sufficiently be able to spot DIB & Bias issues?

## DIB QA Check process:

- [ ] Does any visual content express a visual representation of Diversity? 
- [ ] Does the content show any implicit or explicit bias with regards to gender, race, ethnicity, sexual orientation, diverse-ability, age or any other dimension? 
- [ ] Have all written content or transcripts gone through the inclusive language checker? (To be completed by the creator or Curator)(""Tag the creator/Curator here"")
- [ ] Does the training include reasonable pathways for diverseability team members? (for example close caption options) 
- [ ] Are there any other red or yellow flags in the content written or visual? (If so comment in the issue)

## Does the Learning & Development Program or External Training pass?
- [ ] Yes 
- [ ] No 

## Not passed the DIB QA Check:

**In the comments of this issue be specific as possible:**
- Highlight the time in the video and which video
- Highlight the paragraph and the specific language
- Creator of the content, tag the reviewer once changes have been completed 
