## Diversity, Inclusion, and Belonging Training

[Diversity, Inclusion, and Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) is one of six [GitLab Values](https://about.gitlab.com/handbook/values). This template is intended to be an onboarding introduction to new team members, and a refresher for those who have been with GitLab.

### Steps

- [ ] Assign this issue to the person doing the training with the title of Diversity, Inclusion, and Belonging Training - First Name Last Name - FY##Q#
- [ ] Add the following labels: *Diversity - Inclusion and Belonging*; *Learning and Development* and *DIB LinkedIn Learning Certification*
- [ ] Add a due date to when you are planning to complete the training. 

**IMPORTANT:** Once you complete each certification please make sure to add it, as a comment, to the issue. 

#### Review
  
- [ ] [Diversity, Inclusion, and Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) section of the values page
- [ ] [Diversity, Inclusion, and Belonging page](https://about.gitlab.com/company/culture/inclusion/)
- [ ] [Review the various Team Member Resource Groups (TMRGs)](https://about.gitlab.com/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels)
- [ ] [Being an Ally](https://about.gitlab.com/company/culture/inclusion/being-an-ally/) and take the [knowledge assessment](https://about.gitlab.com/company/culture/inclusion/being-an-ally/#being-an-ally-knowledge-assessment)
- [ ] [Being Inclusive](https://about.gitlab.com/company/culture/inclusion/being-inclusive/) and take the [knowledge assessment](https://about.gitlab.com/company/culture/inclusion/being-inclusive/#inclusion-knowledge-assessment)
- [ ] [Unconscious Bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/)

#### Actions

- [ ] Start learning path [Diversity, Inclusion, and Belonging for all](https://www.linkedin.com/learning/paths/diversity-inclusion-and-belonging-for-all) - approximately 5 hours
  - [ ] Unconscious Bias
  - [ ] Diversity, Inclusion, and Belonging
  - [ ] Confronting Bias: Thriving Across Our Differences
  - [ ] Skills for Inclusive Conversations
  - [ ] Communicating about Culturally Sensitive Issues
  - [ ] Communicating Across Cultures
  - [ ] Bystander Training: From Bystander to Upstander
  - [ ] Upload completion certificate as a comment to this issue 

#### Questions to ask yourself

- How do I present myself among my team members.
- What, if any, did I learn were my unconscious biases.
- Have I considered my privilege(s).
- What actions can I take to promote diversity?
- How can I improve the feeling of inclusivity of those around me?
- Do I have a good sense of how people feel belonging?

#### Self assess

- [ ] In areas of improvement I have identified, I have taken specific action.

#### Closing

- [ ] When you finish the training please ensure that you click on *Close Issue*. 

/label ~"Diversity - Inclusion & Belonging" ~"Learning & Development" ~"DIB LinkedIn Learning Certification"
